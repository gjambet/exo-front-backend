#!/bin/bash

sed -i~ "/<servers>/ a\
<server>\
      <id>registry.hub.docker.com</id>\
      <username>$DOCKER_HUB_USER</username>\
      <password>$DOCKER_HUB_PASSWORD</password>\
</server>" /usr/share/maven/conf/settings.xml
