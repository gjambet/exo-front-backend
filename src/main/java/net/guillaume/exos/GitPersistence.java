package net.guillaume.exos;

import net.guillaume.exos.matches.Goal;
import net.guillaume.exos.matches.Match;
import net.guillaume.exos.matches.MatchRepository;
import net.guillaume.exos.players.Player;
import net.guillaume.exos.players.PlayerRepository;
import net.guillaume.exos.teams.Team;
import net.guillaume.exos.teams.TeamRepository;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class GitPersistence {

    private final TeamRepository teams;
    private final MatchRepository matches;
    private final PlayerRepository players;

    public GitPersistence(TeamRepository teams, MatchRepository matches, PlayerRepository players) {
        this.teams = teams;
        this.matches = matches;
        this.players = players;
    }

    @PostConstruct
    public void initialize() {

        players.save(new Player().withName("Eric Cantona"));
        players.save(new Player().withName("Zinedine Zidane"));
        players.save(new Player().withName("Laurent Blanc"));
        players.save(new Player().withName("Edson Arantes do Nascimento"));

        teams.save(new Team()
                .withName("Manchester United F.C.")
                .withPlayer(new Player().withName("Paul Pogba"))
                .withPlayer(new Player().withName("Marcus Rashford"))
                .withPlayer(new Player().withName("Romelu Lukaku"))
                .withPlayer(new Player().withName("Alexis Sánchez"))
        );

        teams.save(new Team()
                .withName("Juventus F.C.")
                .withPlayer(new Player().withName("Cristiano Ronaldo"))
                .withPlayer(new Player().withName("Paulo Dybala"))
                .withPlayer(new Player().withName("Moise Kean"))
                .withPlayer(new Player().withName("Giorgio Chiellini"))
                .withPlayer(new Player().withName("Mario Mandžukić"))
        );

        teams.save(new Team()
                .withName("Paris Saint-Germain F.C.")
                .withPlayer(new Player().withName("Neymar da Silva Santos Júnior"))
                .withPlayer(new Player().withName("Kylian Mbappé Lottin"))
                .withPlayer(new Player().withName("Edinson Roberto Cavani Gómez"))
                .withPlayer(new Player().withName("Gianluigi Buffon"))
                .withPlayer(new Player().withName("Marco Verratti"))
        );

        matches.save(new Match()
                .withTeamA(teams.findByName("Juventus F.C.").get())
                .withTeamB(teams.findByName("Manchester United F.C.").get())
                .withGoal(new Goal().withPlayer(players.findByName("Cristiano Ronaldo").get()))
                .withGoal(new Goal().withPlayer(players.findByName("Cristiano Ronaldo").get()))
                .withGoal(new Goal().withPlayer(players.findByName("Paulo Dybala").get()))
                .withGoal(new Goal().withPlayer(players.findByName("Paul Pogba").get()))
        );
    }

}
