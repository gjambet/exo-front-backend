package net.guillaume.exos.teams;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@CrossOrigin
public class TeamController {

    private final TeamRepository teams;

    public TeamController(TeamRepository teams) {
        this.teams = teams;
    }

    @GetMapping(value = "/teams")
    public Iterable<Team> players() {
        return teams.findAll();
    }

    @GetMapping(value = "/teams/{id}")
    public Optional<Team> task(@PathVariable Long id) {
        return teams.findById(id);
    }

}
