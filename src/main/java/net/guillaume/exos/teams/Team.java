package net.guillaume.exos.teams;


import net.guillaume.exos.players.Player;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.AUTO;

@Entity
public class Team implements Serializable {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    private String name;

    @OneToMany(cascade = ALL)
    private List<Player> players;

    public Team() {
        players = new ArrayList<>();
        // JPA
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Team withName(String s) {
        setName(s);
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Team withPlayer(Player player) {
        players.add(player);
        return this;
    }
}
