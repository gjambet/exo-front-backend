package net.guillaume.exos.matches;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@CrossOrigin
public class MatchController {

    private final MatchRepository teams;

    public MatchController(MatchRepository teams) {
        this.teams = teams;
    }

    @GetMapping(value = "/matches")
    public Iterable<Match> players() {
        return teams.findAll();
    }

    @GetMapping(value = "/matches/{id}")
    public Optional<Match> task(@PathVariable Long id) {
        return teams.findById(id);
    }

}
