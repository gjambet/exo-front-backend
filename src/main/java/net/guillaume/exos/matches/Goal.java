package net.guillaume.exos.matches;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.guillaume.exos.players.Player;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

import static javax.persistence.GenerationType.AUTO;

@Entity
public class Goal implements Serializable {

    @Id
    @GeneratedValue(strategy = AUTO)
    @JsonIgnore
    private Long id;

    private Long playerId;

    public Goal() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Goal withPlayer(Player player){
        playerId = player.getId();
        return this;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }
}
