package net.guillaume.exos.matches;


import net.guillaume.exos.teams.Team;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.AUTO;

@Entity
public class Match implements Serializable {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;

    private Long teamA;
    private Long teamB;

    @OneToMany(cascade = ALL)
    private List<Goal> goals;

    public Match() {
        goals = new ArrayList<>();
    }

    public Match withTeamA(Team teamA) {
        setTeamA(teamA.getId());
        return this;
    }

    public Match withTeamB(Team teamB) {
        setTeamB(teamB.getId());
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTeamA() {
        return teamA;
    }

    public void setTeamA(Long teamA) {
        this.teamA = teamA;
    }

    public Long getTeamB() {
        return teamB;
    }

    public void setTeamB(Long teamB) {
        this.teamB = teamB;
    }

    public List<Goal> getGoals() {
        return goals;
    }

    public void setGoals(List<Goal> goals) {
        this.goals = goals;
    }

    public Match withGoal(Goal goal) {
        goals.add(goal);
        return this;
    }
}
