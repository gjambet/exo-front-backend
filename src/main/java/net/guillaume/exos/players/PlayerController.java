package net.guillaume.exos.players;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@CrossOrigin
public class PlayerController {

    private PlayerRepository players;

    public PlayerController(PlayerRepository players) {
        this.players = players;
    }

    @GetMapping(value = "/players")
    public Iterable<Player> players() {
        return players.findAll();
    }

    @GetMapping(value = "/players/{id}")
    public Optional<Player> task(@PathVariable Long id) {
        return players.findById(id);
    }

}
