package net.guillaume.exos.players;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

import static javax.persistence.GenerationType.AUTO;

@Entity
public class Player implements Serializable {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    private String name;

    public Player() {
        // JPA
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Player withName(String name) {
        setName(name);
        return this;
    }

    @JsonProperty("self")
    public String self() {
        return "uefa-root-public-url/players/" + id;
    }

}
