package net.guillaume.exos.favorites;

public class FavoriteCreatedResponse {

    private final FavoriteRequest favoriteRequest;

    public FavoriteCreatedResponse(FavoriteRequest favoriteRequest) {
        this.favoriteRequest = favoriteRequest;
    }

    public Integer getPlayer() {
        return favoriteRequest.getPlayer();
    }

    public Integer getTeam() {
        return favoriteRequest.getTeam();
    }
}
