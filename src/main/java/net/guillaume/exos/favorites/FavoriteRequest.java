package net.guillaume.exos.favorites;

public class FavoriteRequest {

    private Integer player;
    private Integer team;

    public FavoriteRequest(Integer player, Integer team) {
        this.player = player;
        this.team = team;
    }

    public Integer getPlayer() {
        return player;
    }

    public void setPlayer(Integer player) {
        this.player = player;
    }

    public Integer getTeam() {
        return team;
    }

    public void setTeam(Integer team) {
        this.team = team;
    }

}
