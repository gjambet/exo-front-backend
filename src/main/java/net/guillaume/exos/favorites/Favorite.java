package net.guillaume.exos.favorites;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

import static javax.persistence.GenerationType.AUTO;

@Entity
public class Favorite implements Serializable {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    private String name;


}
