package net.guillaume.exos.favorites;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@CrossOrigin
public class FavoriteController {

    private Favorite favorite;

    @GetMapping(value = "/me/favorites")
    public Optional<Favorite> favorites() {
        return Optional.of(favorite);
    }

    @PostMapping(value = "/me/favorites")
    public FavoriteCreatedResponse task(@RequestBody Favorite favorite) {
        return new FavoriteCreatedResponse(new FavoriteRequest(1, 2));
    }

}
