

# Try me with : 

* locally with 

mvn clean package spring-boot:run

* using a docker image : 

https://cloud.docker.com/repository/docker/trollheim/exo-front-backend

docker pull trollheim/exo-front-backend:1.0-SNAPSHOT
docker stop exo-front-backend
docker rm  exo-front-backend
docker create -p80:8080 --name exo-front-backend trollheim/exo-front-backend:1.0-SNAPSHOT
docker start exo-front-backend


you can use the following apis : 
http://51.254.102.101:558/players
http://51.254.102.101:558/teams
http://51.254.102.101:558/matches

Goal of the exercise is to display the list of all players 

for each row : player's name, player's current team , Total number of goals he scored

this must looks good on both a phone and a computer

sorted by number of goals
